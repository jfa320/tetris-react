import {useState,useEffect,useCallback} from 'react';

export const useGameStatus=rowsCleared=>{
    const [score,setScore]=useState(0);
    const [rows,setRows]=useState(0);
    const [level,setLevel]=useState(0);

    const linePoints=[40,100,300,1200]; //puntos correspondientes por cant lineas (de acuerdo a wikipedia)

    const calcScore=useCallback(
        () => {
            if(rowsCleared>0){
                //calculo que usa el Tetris original
                setScore(prev=>prev+linePoints[rowsCleared-1]*(level+1));
                setRows(prev=>prev+rowsCleared);
            }
        },
        [level,linePoints,rowsCleared]
    )
    useEffect(() => {
        calcScore();
    }, [calcScore,level,rowsCleared]);

    return [score,setScore,rows,setRows,level,setLevel];
}