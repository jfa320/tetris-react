import React,{Fragment, useState} from 'react';
import {createStage,checkCollision} from '../gameHelpers';

//Componentes
import Stage from './Stage';
import Display from './Display';
import StartButton from './StartButton';


//Styled Components
import {StyledTetrisWrapper,StyledTetris} from './styles/StyledTetris';

//Hooks
import {usePlayer} from '../hooks/usePlayer';
import {useStage} from '../hooks/useStage';
import {useInterval} from '../hooks/useInterval';
import {useGameStatus} from '../hooks/useGameStatus';


const Tetris=()=>{
    const [dropTime,setDropTime]=useState(null);
    const [gameover,setGameOver]=useState(false);

    const [player,updatePlayerPos,resetPlayer,playerRotate]=usePlayer();
    const [stage,setStage,rowsCleared]=useStage(player,resetPlayer);
    const[score,setScore,rows,setRows,level,setLevel]=useGameStatus(rowsCleared);

    console.log('re-render');
    const movePlayer= dir=>{  //solo mueve a derecha o izquierda
        if(!checkCollision(player,stage,{x:dir,y:0})){
            updatePlayerPos({x:dir,y:0});
        }
    }
    const startGame=()=>{
        //reset everything
        setStage(createStage());
        resetPlayer();
        setDropTime(1000);//1 segundo
        setGameOver(false);
        setScore(0);
        setRows(0);
        setLevel(0);
    }
    const drop =()=>{ //muevo hacia abajo
        //subir nivel cuando limpio diez filas 
        if(rows>(level+1)*10){
            setLevel(prev=>prev+1);
            //subir velocidad
            setDropTime(1000/(level+1)+200);
        }
        if(!checkCollision(player,stage,{x:0,y:1})){
            updatePlayerPos({x:0,y:1,collided:false});
        }else{
            //game over
            if(player.pos.y<1){
                console.log("Game over");
                setGameOver(true);
                setDropTime(null);
            }
            updatePlayerPos({x:0,y:0,collided:true})
        }
    }

    const keyUp=({keyCode})=>{
        if(!gameover){
            console.log(keyCode);
            if(keyCode===40 ){
                setDropTime(1000/(level+1)+200);                           
            }
        }
    }
    
    const dropPlayer =()=>{ 
        setDropTime(null);
        drop();
    }
    const move=({keyCode})=>{ //keyCode es un atributo de e (evento) => aca hago una destructuracion y me quedo     solo con ese valor de e
        if (!gameover){
            if(keyCode===37){ //37 es el ascii para flecha izquierda en teclado
                movePlayer(-1);
            }else if(keyCode===39){//39 es el ascii para flecha izquierda en teclado
                movePlayer(1);
            }else if (keyCode===40 ){ //40 es el ascii para flecha abajo en teclado 
                dropPlayer();
            }else if(keyCode===38 || keyCode===65){ //38 para flecha arriba | 65 para letra a 
                playerRotate(stage,1);
            }
        }
    }

    useInterval(()=>{
        drop();
    },dropTime);
    console.log(stage);
    return (
        <StyledTetrisWrapper 
            role="button" 
            tabIndex="0" 
            onKeyDown={e=>move(e)} 
            onKeyUp={keyUp}
        >
             <StyledTetris>
                <Stage stage={stage}/>
                <aside>
                    {gameover ? (
                        <Display gameover={gameover} text="Game over"/>
                    ):(
                        <div>
                            <Display text={`Puntaje: ${score}`}/>
                            <Display text={`Filas: ${rows}`} />
                            <Display text={`Nivel: ${level}`} />
                        </div>
                    )}
                    <StartButton callback={startGame}/>
                </aside>
            </StyledTetris>

        </StyledTetrisWrapper>
    )
}
export default Tetris;