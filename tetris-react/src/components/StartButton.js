import React from 'react';
import {StyledStartButton} from './styles/StyledStartButton';

const StartButton=({callback})=>(
    <StyledStartButton onClick={callback}>Iniciar juego</StyledStartButton>
)
export default StartButton;