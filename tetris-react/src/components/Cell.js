import React from 'react';
import {StyledCell} from './styles/StyledCell';
import {TETROMINOS} from '../tetrominos';

const Cell=({type}) =>(
    <StyledCell type={type} color={TETROMINOS[type].color}/>
)

export default React.memo(Cell); //con react memo solo renderizo cuando el componente cambia a la version anterior