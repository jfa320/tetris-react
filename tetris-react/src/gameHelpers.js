export const STAGE_WIDTH=12;
export const STAGE_HEIGHT=20;

export const createStage= () => Array.from(Array(STAGE_HEIGHT),()=>
    new Array(STAGE_WIDTH).fill([0,'clear'])
);

export const checkCollision= (player,stage,{x:moveX,y:moveY})=>{
    for(let y=0;y<player.tetromino.length;y++){
        for(let x=0;x<player.tetromino[y].length;x++){
            //chequeo que estoy en un tetromino
            if(player.tetromino[y][x]!==0){
                if(
                    //chequeo que este adentro del area del stage  (y - height)
                    !stage[y+player.pos.y+moveY] ||
                    //chequeo que este adentro del area del stage  (x - width)
                    !stage[y+player.pos.y+moveY][x+player.pos.x+moveX] ||    
                    //chequeo que la celda no esta seteada en vacia (clear)
                    stage[y+player.pos.y+moveY][x+player.pos.x+moveX][1]!=="clear"  
                ){
                    return true;
                }
            }
        }
    }
}